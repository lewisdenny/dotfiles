""Fix indentation for yaml files
autocmd FileType yaml setlocal ai ts=2 sw=2 et

""TODO: Would be nice to have a 1 second popup confirming the link has been sent to the clipboard
""TODO: Test if in root directory to avoid // after branch name: https://gitlab.com/lewisdenny/cas/blob/master//cas#L200
""Line link generator for opendev.org
nnoremap <leader>od :!echo `git config --get remote.origin.url\|sed "s/.\{4\}$//"`/src/`git rev-parse --abbrev-ref HEAD`/`pwd \| sed "s?$(git rev-parse --show-toplevel)??"`/%\#L<C-R>=line('.')<CR>\| xclip -selection c<CR><CR>

""Line link generator for github and gitlab
nnoremap <leader>ll :!echo `git config --get remote.origin.url\|sed "s/.\{4\}$//"`/blob/`git rev-parse --abbrev-ref HEAD`/`pwd \| sed "s?$(git rev-parse --show-toplevel)??"`/%\#L<C-R>=line('.')<CR>\| xclip -selection c<CR><CR>


